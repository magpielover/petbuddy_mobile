const articles = [
  {
    id: 1,
    photo: require("../img/dog1.jpg"),
    type: "article",
    time: -300,
    header: "Bobby",
    text:
      "Bobby is a very calm dog. He likes to go to dog park and he gets along with other dogs.",

    comments: [
      {
        id: 1,
        text:
          "Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
        time: 0
      },
      {
        id: 2,
        text: "Quisque ut erat. Curabitur gravida nisi at nibh.",
        time: -311
      },
      {
        id: 3,
        text:
          "Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.",
        time: -622
      },
      {
        id: 4,
        text: "In est risus, auctor sed, tristique in, tempus sit amet, sem.",
        time: -933
      },
      {
        id: 5,
        text: "In hac habitasse platea dictumst.",
        time: -1244
      },
      {
        id: 6,
        text:
          "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
        time: -1555
      },
      {
        id: 7,
        text:
          "Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.",
        time: -1866
      },
      {
        id: 8,
        text:
          "Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        time: -2177
      }
    ]
  },
  {
    id: 2,
    photo: require("../img/dog2.jpg"),
    type: "article",
    time: -1373,
    header: "Lenni",
    text: "Lenni is so smart and curious.",
    comments: []
  },
  {
    id: 3,
    photo: require("../img/dog3.jpg"),
    type: "article",
    time: -2446,
    header: "Charlie",
    text: "Charlie is so independent.",
    comments: []
  },
  {
    id: 4,
    photo: require("../img/dog4.jpg"),
    type: "article",
    time: -3519,
    header: "Kirsikka",
    text:
      "Kirsikka is a very smart dog. She knows exactly how to behave in public.",
    comments: []
  },
  {
    id: 5,
    photo: require("../img/dog5.jpg"),
    type: "article",
    time: -4592,
    header: "Lenni",
    text: "Lenni can be agressive if you approach to his food.",
    comments: []
  },
  {
    id: 6,
    photo: require("../img/dog1.jpg"),
    type: "article",
    time: -5665,
    header: "Mountains",
    text:
      "Mountains make up about one-fifth of the world's landscape, and provide homes to at least one-tenth of the world's people. " +
      "The tallest known mountain in the solar system is Olympus Mons, located on Mars. " +
      "There are mountains under the surface of the sea! " +
      "Mountains occur more often in oceans than on land; some islands are the peaks of mountains coming out of the water.",
    comments: []
  },
  {
    id: 7,
    photo: require("../img/dog2.jpg"),
    type: "fact",
    time: -5665,
    header: "Smile and Frown",
    text: "It takes 17 muscles to smile and 43 to frown.",
    comments: []
  },
  {
    id: 8,
    photo: require("../img/dog5.jpg"),
    type: "fact",
    time: -8373,
    header: "Interesting Fact",
    text: "Dolphins sleep with one eye open.",
    comments: []
  },
  {
    id: 9,
    photo: require("../img/dog3.jpg"),
    type: "fact",
    time: -565,
    header: "Elephant",
    text: "Elephant is one of the few mammals that can't jump.",
    comments: []
  },
  {
    id: 10,
    photo: require("../img/photo48.png"),
    type: "fact",
    time: -52365,
    header: "Cold Water",
    text: "Cold water weighs less than hot water.",
    comments: []
  },
  {
    id: 11,
    photo: require("../img/photo49.png"),
    type: "fact",
    time: -1295,
    header: "Our Eyes",
    text: "You blink over 10,000,000 times a year.",
    comments: []
  },
  {
    id: 12,
    photo: require("../img/photo17.png"),
    type: "post",
    time: -300,
    title: "My Little Kitten",
    text:
      "I have got a cat. Her name is Matilda. She is quite old for a cat. She is eleven years old. Matilda is very" +
      " fluffy. Her back is black and her belly and chest are white. She also has a black muzzle with long white whiskers. " +
      "Her legs and paws are white. Matilda has big eyes. Her eyes are light green, but they become yellow in bright sunlight. I love my cat.",
    comments: []
  },
  {
    id: 13,
    photo: require("../img/photo18.png"),
    type: "post",
    time: -1373,
    header: "Interesting Fact",
    text:
      "One chef prepared a delicious cake with apples and named it in honor of his beloved Charlotte.",
    comments: []
  },
  {
    id: 14,
    photo: require("../img/photo19.png"),
    type: "post",
    time: -2446,
    header: "Music In Our Life",
    text:
      "The scientists say that they can define your character if they know what music you like.",
    comments: []
  },
  {
    id: 15,
    photo: require("../img/photo20.png"),
    type: "post",
    time: -3519,
    header: "Exciting Adventure",
    text:
      "My trip to Spain last summer. I think that it was the most interesting trip in my life.",
    comments: []
  }
];

export default articles;
