import React from "react";
import { ScrollView, View, StyleSheet } from "react-native";
import {
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkTheme,
  RkStyleSheet
} from "react-native-ui-kitten";
import { data } from "../../data";
import { Avatar, SocialSetting, GradientButton } from "../../components";
import { FontAwesome } from "../../assets/icons";
const restApi = "http://dogsitter.ugurkirbac.fi:1337";
export class ProfileSettings extends React.Component {
  static navigationOptions = {
    title: "Profile Settings".toUpperCase()
  };

  constructor(props) {
    super(props);
    user = data.getUser();
    let us = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      country: user.country,
      phone: user.phone,
      password: user.password,
      newPassword: user.newPassword,
      confirmPassword: user.confirmPassword,
      photo: user.photo
    };
    this.state = { user: us };
    this.updateProfile = this.updateProfile.bind(this);
  }

  componentDidMount() {
    this.fetchUser();
  }
  fetchUser = () => {
    const url = restApi + "/api/v1/users/1";

    fetch(url)
      .then(response => response.json())
      .then(responseJson => {
        console.log("response ne" + JSON.stringify(responseJson));
        if (responseJson.length) {
          this.populateUserData(responseJson[0]);
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  populateUserData = data => {
    let user = {};
    console.log("USER NE" + JSON.stringify(data));
    user.email = data.emailAddress;
    user.firstName = data.fullName;
    user.phone = data.phone;
    user.country = data.country;
    user.password = data.password;
    user.newPassword = data.password;
    user.confirmPassword = data.password;
    user.photo = this.state.user.photo;
    this.setState({ user: user });
  };

  onFirstNameInputChanged = text => {
    let a = this.state.user;
    a.fullName = text;
    this.setState({ user: a });
  };

  onLastNameInputChanged = text => {
    this.setState({ lastName: text });
  };

  onEmailInputChanged = text => {
    let a = this.state.user;
    a.email = text;
    this.setState({ user: a });
  };

  onCountryInputChanged = text => {
    let a = this.state.user;
    a.country = text;
    this.setState({ user: a });
  };

  onPhoneInputChanged = text => {
    let a = this.state.user;
    a.phone = text;
    this.setState({ user: a });
  };

  onPasswordInputChanged = text => {
    let a = this.state.user;
    a.password = text;
    this.setState({ user: a });
  };

  onNewPasswordInputChanged = text => {
    this.setState({ newPassword: text });
  };

  onConfirmPasswordInputChanged = text => {
    this.setState({ confirmPassword: text });
  };

  updateProfile() {
    const url = restApi + "/api/v1/users/1";
    try {
      fetch(url, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(this.state.user)
      });
    } catch (error) {
      console.error(error);
    }
  }
  render = () => (
    <ScrollView style={styles.root}>
      <RkAvoidKeyboard>
        <View style={styles.header}>
          <Avatar img={this.state.user.photo} rkType="big" />
        </View>
        <View style={styles.section}>
          <View style={[styles.row, styles.heading]}>
            <RkText rkType="header6 primary">INFO</RkText>
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="First Name"
              value={this.state.user.firstName}
              rkType="right clear"
              onChangeText={this.onFirstNameInputChanged}
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Last Name"
              value={this.state.user.lastName}
              onChangeText={this.onLastNameInputChanged}
              rkType="right clear"
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Email"
              value={this.state.user.email}
              onChangeText={this.onEmailInputChanged}
              rkType="right clear"
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Country"
              value={this.state.user.country}
              onChangeText={this.onCountryInputChanged}
              rkType="right clear"
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Phone"
              value={this.state.user.phone}
              onChangeText={this.onPhoneInputChanged}
              rkType="right clear"
            />
          </View>
        </View>
        <View style={styles.section}>
          <View style={[styles.row, styles.heading]}>
            <RkText rkType="primary header6">CHANGE PASSWORD</RkText>
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Old Password"
              value={this.state.user.password}
              rkType="right clear"
              secureTextEntry
              onChangeText={this.onPasswordInputChanged}
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="New Password"
              value={this.state.user.newPassword}
              rkType="right clear"
              secureTextEntry
              onChangeText={this.onNewPasswordInputChanged}
            />
          </View>
          <View style={styles.row}>
            <RkTextInput
              label="Confirm Password"
              value={this.state.user.confirmPassword}
              rkType="right clear"
              secureTextEntry
              onChangeText={this.onConfirmPasswordInputChanged}
            />
          </View>
        </View>
        <View style={styles.section}>
          <View style={[styles.row, styles.heading]}>
            <RkText rkType="primary header6">CONNECT YOUR ACCOUNT</RkText>
          </View>
          <View style={styles.row}>
            <SocialSetting
              name="Twitter"
              icon={FontAwesome.twitter}
              tintColor={RkTheme.current.colors.twitter}
            />
          </View>
          <View style={styles.row}>
            <SocialSetting
              name="Google"
              icon={FontAwesome.google}
              tintColor={RkTheme.current.colors.google}
            />
          </View>
          <View style={styles.row}>
            <SocialSetting
              name="Facebook"
              icon={FontAwesome.facebook}
              tintColor={RkTheme.current.colors.facebook}
            />
          </View>
        </View>
        <GradientButton
          rkType="large"
          style={styles.button}
          onPress={this.updateProfile}
          text="SAVE"
        />
      </RkAvoidKeyboard>
    </ScrollView>
  );
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base
  },
  header: {
    backgroundColor: theme.colors.screen.neutral,
    paddingVertical: 25
  },
  section: {
    marginVertical: 25
  },
  heading: {
    paddingBottom: 12.5
  },
  row: {
    flexDirection: "row",
    paddingHorizontal: 17.5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base,
    alignItems: "center"
  },
  button: {
    marginHorizontal: 16,
    marginBottom: 32
  }
}));
