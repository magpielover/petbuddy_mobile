import React from "react";
import {
  View,
  Image,
  Dimensions,
  Keyboard,
  AsyncStorage,
  Alert
} from "react-native";
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme
} from "react-native-ui-kitten";
import { FontAwesome } from "../../assets/icons";
import { GradientButton } from "../../components/gradientButton";
import { scaleModerate, scaleVertical } from "../../utils/scale";
import NavigationType from "../../config/navigation/propTypes";
import deviceStorage from "../../utils/deviceStorage";

const restApi = "http://dogsitter.ugurkirbac.fi:1337";
export class LoginV1 extends React.Component {
  constructor(props) {
    super(props);
    state = { email: "", password: "" };
  }
  static propTypes = {
    navigation: NavigationType.isRequired
  };
  static navigationOptions = {
    header: null
  };

  getThemeImageSource = theme =>
    theme.name === "light"
      ? require("../../assets/images/backgroundLoginV1.png")
      : require("../../assets/images/backgroundLoginV1DarkTheme.png");

  renderImage = () => {
    const screenSize = Dimensions.get("window");
    const imageSize = {
      width: screenSize.width,
      height: screenSize.height - scaleModerate(375, 1)
    };
    return (
      <Image
        style={[styles.image, imageSize]}
        source={this.getThemeImageSource(RkTheme.current)}
      />
    );
  };
  async loginWithCredentials() {
    const url = restApi + "/api/v1/login";
    try {
      fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          emailAdress: this.state.email,
          password: this.state.password
        })
      });
      let responseJson = await response.json();
      console.log("Gelen Token:" + responseJson.token);
      deviceStorage.saveItem("id_token", responseJson.token);
    } catch (error) {
      console.error(error);
    }
  }
  onLoginButtonPressed = () => {
    this.loginWithCredentials();
    this.props.navigation.navigate("App");
  };

  onSignUpButtonPressed = () => {
    this.props.navigation.navigate("SignUp");
  };

  async logIn() {
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
      "279216279656740",
      {
        permissions: ["public_profile"]
      }
    );
    deviceStorage.saveItem("fb_token", token);
    console.log("Facebook token ne: " + token);
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      const jwt_token = await fetch(
        `http://dogsitter.ugurkirbac.fi/api/v1/facebook?access_token=${token}`
      );
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      deviceStorage.saveItem("jwt_token", jwt_token);
      Alert.alert("Logged in!", `Hi ${(await response.json()).name}!`);
      console.log("Facebook response: " + JSON.stringify(response.json()));
      this.props.navigation.navigate("App");
    }
  }
  onEmailChanged = email => {
    this.setState({ email: email });
  };
  onPasswordChanged = password => {
    this.setState({ password: password });
  };
  render = () => (
    <RkAvoidKeyboard
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}
      style={styles.screen}
    >
      {this.renderImage()}
      <View style={styles.container}>
        <RkTextInput
          rkType="rounded"
          placeholder="Email"
          onChange={this.onEmailChanged}
        />
        <RkTextInput
          rkType="rounded"
          placeholder="Password"
          onChange={this.onPasswordChanged}
          secureTextEntry
        />
        <GradientButton
          style={styles.save}
          rkType="large"
          onPress={this.onLoginButtonPressed}
          text="LOGIN"
        />
        <GradientButton
          style={styles.save}
          colors={["#4c669f", "#3b5998", "#192f6a"]}
          rkType="medium"
          onPress={this.logIn}
          text="Login with Facebook"
        />
        <View style={styles.footer}>
          <View style={styles.textRow}>
            <RkText rkType="primary3">Don’t have an account?</RkText>
            <RkButton rkType="clear">
              <RkText rkType="header6" onPress={this.onSignUpButtonPressed}>
                Sign up now
              </RkText>
            </RkButton>
          </View>
        </View>
      </View>
    </RkAvoidKeyboard>
  );
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    flex: 1,
    alignItems: "center",
    backgroundColor: theme.colors.screen.base
  },
  image: {
    resizeMode: "cover",
    marginBottom: scaleVertical(10)
  },
  container: {
    paddingHorizontal: 17,
    paddingBottom: scaleVertical(22),
    alignItems: "center",
    flex: -1
  },
  footer: {
    justifyContent: "flex-end",
    flex: 1
  },
  buttons: {
    flexDirection: "row",
    marginBottom: scaleVertical(24)
  },
  button: {
    marginHorizontal: 14
  },
  save: {
    marginVertical: 9
  },
  textRow: {
    justifyContent: "center",
    flexDirection: "row"
  }
}));
